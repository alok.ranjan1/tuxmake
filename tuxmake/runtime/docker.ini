# Lists of images to build. Each image needs to have a corresponding section
# below.
[runtime]
bases = base-debian10, base-debian11, base-debian12, base-debiantesting, base-debianunstable
toolchains =
    gcc,
    gcc-8,
    gcc-9,
    gcc-10,
    gcc-11,
    gcc-12,
    gcc-13,
    gcc-14,
    clang,
    clang-10,
    clang-11,
    clang-12,
    clang-13,
    clang-14,
    clang-15,
    clang-16,
    clang-17,
    clang-18,
    clang-19,
    clang-nightly,
    clang-android,
    llvm,
    llvm-10,
    llvm-11,
    llvm-12,
    llvm-13,
    llvm-14,
    llvm-15,
    llvm-16,
    llvm-17,
    llvm-18,
    llvm-19,
    llvm-nightly,
    llvm-android,
    rust,
    rustgcc,
    rustclang,
    rustllvm,
    korg-clang,
    korg-clang-11,
    korg-clang-12,
    korg-clang-13,
    korg-clang-14,
    korg-clang-15,
    korg-clang-16,
    korg-clang-17,
    korg-clang-18,
    korg-clang-19,
    korg-llvm,
    korg-llvm-11,
    korg-llvm-12,
    korg-llvm-13,
    korg-llvm-14,
    korg-llvm-15,
    korg-llvm-16,
    korg-llvm-17,
    korg-llvm-18,
    korg-llvm-19,

# Image specifications.
#
# The following fields are mandatory:
#
# - kind: kind of container. The kind must be implemented in
#   support/docker/configure
#
# - base: base image. for images with kind = "base", can be any image. for the
#   others, needs to be an existing image defined in this file.
#
# - hosts: the hosts where this image is to be built. Most images should build
#   on amd64 and arm64 (the default)
#
# - rebuild: when to rebuild this image. currently only monthly is supported.
#
# Optional fields:
#
# - target_bases: alternative base images for some targets. Format:
#   comma-separated list; each item must be <targetarch>:<baseimage>.
#   Example:

#   target_bases = riscv:gcc-10, arc:gcc-9
#
# - target_kinds = alternative image kind for some targets. Format:
#   comma-separated list; each item must be <targetarch>:<kind>
#   Example:

#   target_kinds = arc:arc, xxx:yyy
#
# - target_hosts = list of hosts where to build image for this specific target.
#   Format: comma separated list; each item is a list separated by +.
#   Example:
#   target_hosts = mips:amd64, arc:amd64, arm64:amd64+arm64

[base-debian10]
skip_build = True
kind    = base
base    = docker.io/library/debian:10-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debian11]
kind    = base
base    = docker.io/library/debian:bullseye-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debian12]
kind    = base
base    = docker.io/library/debian:bookworm-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debiantesting]
kind    = base
base    = docker.io/library/debian:testing-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debianunstable]
kind    = base
base    = docker.io/library/debian:unstable-slim
hosts   = amd64, arm64
rebuild = monthly

[gcc]
kind    = gcc-build
base    = base-debian11
hosts   = amd64, arm64
rebuild = monthly
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
target_bases = riscv:gcc-10, arc:gcc-9
target_kinds = arc:arc
target_hosts = mips:amd64, arc:amd64, parisc:amd64, powerpc:amd64, s390:amd64, sh:amd64, sparc:amd64
packages = gcc, g++

[gcc-8]
skip_build = True
kind = gcc-build
base = base-debian10
hosts = amd64, arm64
rebuild = monthly
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
target_skip = armv5, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
target_kinds = arc:arc
target_hosts = mips:amd64, riscv:amd64, arc:amd64, parisc:amd64, powerpc:amd64, s390:amd64, sh:amd64, sparc:amd64
packages = gcc-8, g++-8

[gcc-9]
kind = gcc-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
rebuild = monthly
target_kinds = arc:arc
target_hosts = arc:amd64, riscv:amd64, parisc:amd64, powerpc:amd64, sh:amd64, sparc:amd64
target_skip = mips
packages = gcc-9, g++-9

[gcc-10]
kind = gcc-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, parisc, powerpc, s390, sh, sparc
target_hosts = parisc:amd64, powerpc:amd64, sh:amd64, sparc:amd64
rebuild = monthly
packages = gcc-10, g++-10

[gcc-11]
kind = gcc-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, riscv, parisc, powerpc, s390, sh, sparc
target_hosts = parisc:amd64, powerpc:amd64, sh:amd64, sparc:amd64
target_bases = riscv:base-debianunstable, parisc:base-debianunstable, sh:base-debianunstable, sparc:base-debianunstable
rebuild = monthly
packages = gcc-11, g++-11

[gcc-12]
kind = gcc-build
base = base-debian12
hosts = amd64, arm64
# targets: no parisc, sh, sparc for now
targets = x86_64, arm64, i386, arm, armv5, m68k, mips, powerpc, riscv, s390
target_hosts = m68k:amd64, parisc:amd64, powerpc:amd64, sh:amd64, sparc:amd64
target_bases = riscv:base-debianunstable
rebuild = monthly
packages = gcc-12, g++-12

[gcc-13]
kind = gcc-build
base = base-debiantesting
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, powerpc, riscv, m68k, parisc, sh, sparc, s390
# target_hosts = m68k:amd64, parisc:amd64, sh:amd64, sparc:amd64
rebuild = monthly
packages = gcc-13, g++-13
boot_test = False

[gcc-14]
kind = gcc-build
base = base-debiantesting
hosts = amd64, arm64
# targets: no mips for now, see target_skip
targets = x86_64, arm64, arm, armv5, i386, mips, powerpc, riscv, m68k, parisc, sh, sparc, s390
target_skip = mips
rebuild = monthly
packages = gcc-14, g++-14
boot_test = False

[clang]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang, clang-tidy, llvm, lld
target_hosts = mips:amd64

[clang-10]
skip_build = True
kind = clang-build
base = base-debian10
hosts = amd64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
target_skip = armv5, mips, riscv, powerpc, s390, sparc
rebuild = monthly
packages = clang-10, clang-tidy-10, llvm-10, lld-10
target_hosts = mips:amd64
extra_apt_repo = deb http://apt.llvm.org/buster/ llvm-toolchain-buster-10 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key


[clang-11]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang-11, clang-tidy-11, llvm-11, lld-11
target_hosts = mips:amd64

[clang-12]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang-12, clang-tidy-12, llvm-12, lld-12
target_hosts = mips:amd64
install_options = -t llvm-toolchain-bullseye-12
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-12 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-13]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang-13, clang-tidy-13, llvm-13, lld-13
target_hosts = mips:amd64
install_options = -t llvm-toolchain-bullseye-13
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-13 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-14]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang-14, clang-tidy-14, libclang-rt-14-dev, llvm-14, lld-14
target_hosts = mips:amd64
install_options = -t llvm-toolchain-bullseye-14
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-14 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-15]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang-15, clang-tidy-15, libclang-rt-15-dev, llvm-15, lld-15
target_hosts = mips:amd64
target_bases = riscv:base-debianunstable
install_options = -t llvm-toolchain-bullseye-15
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-15 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-16]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390
rebuild = monthly
packages = clang-16, clang-tidy-16, libclang-rt-16-dev, llvm-16, lld-16
target_hosts = mips:amd64
target_bases = riscv:base-debianunstable
install_options = -t llvm-toolchain-bullseye-16
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-16 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-17]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, riscv, powerpc, s390, loongarch
rebuild = monthly
packages = clang-17, clang-tidy-17, libclang-rt-17-dev, llvm-17, lld-17
target_hosts = mips:amd64
target_bases = riscv:base-debianunstable
install_options = -t llvm-toolchain-bullseye-17
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-17 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-18]
kind = clang-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, powerpc, riscv, s390, loongarch
rebuild = monthly
packages = clang-18, clang-tidy-18, libclang-rt-18-dev, llvm-18, lld-18
target_bases = riscv:base-debianunstable
install_options = -t llvm-toolchain-bullseye-18
extra_apt_repo = deb http://apt.llvm.org/bullseye llvm-toolchain-bullseye-18 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-19]
kind = clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, powerpc, riscv, s390, loongarch
rebuild = monthly
packages = clang-19, clang-tidy-19, libclang-rt-19-dev, llvm-19, lld-19,
target_bases = riscv:base-debianunstable, loongarch:base-debianunstable
# Skip the following targets, since libfuse is unavailable as of now
target_skip = riscv, loongarch
install_options = -t llvm-toolchain-bookworm-19
extra_apt_repo = deb http://apt.llvm.org/bookworm llvm-toolchain-bookworm-19 main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-nightly]
kind = clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, armv5, mips, powerpc, riscv, s390, loongarch
rebuild = daily
packages = clang-20, clang-tidy-20, libclang-rt-20-dev, llvm-20, lld-20
install_options = -t llvm-toolchain-bookworm
extra_apt_repo = deb http://apt.llvm.org/bookworm llvm-toolchain-bookworm main
extra_apt_repo_key = https://apt.llvm.org/llvm-snapshot.gpg.key

[clang-android]
kind = clang-android-build
base = base-debian11
# LLVM android binaries are x86-only:
hosts = amd64
targets = x86_64, arm64, i386, arm, riscv
rebuild = monthly

[rust]
kind = rust
base = base-debiantesting
packages = gcc, g++
hosts = amd64, arm64
targets = x86_64, arm64, arm, armv5, powerpc, riscv
rebuild = monthly

[korg-clang-11]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 11.1.0
rebuild = monthly

[korg-clang-12]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 12.0.1
rebuild = monthly

[korg-clang-13]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 13.0.1
rebuild = monthly

[korg-clang-14]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 14.0.6
rebuild = monthly

[korg-clang-15]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 15.0.7
rebuild = monthly

[korg-clang-16]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 16.0.6
rebuild = monthly

[korg-clang-17]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390
tc_full_version = 17.0.6
rebuild = monthly

[korg-clang-18]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390, loongarch
target_hosts = loongarch:amd64
tc_full_version = 18.1.8
rebuild = monthly

[korg-clang-19]
kind = korg-clang-build
base = base-debian12
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390, loongarch
target_hosts = loongarch:amd64
tc_full_version = 19.1.0-rc3
rebuild = monthly
