strict: true
site_name: TuxMake
site_description: |
  Documentation for TuxMake, a command line tool and Python library utility
  that provides portable and repeatable Linux kernel builds across a variety of
  architectures, toolchains, kernel configurations, and make targets.
site_author: Tuxmake Team
site_dir: public
repo_url: https://gitlab.com/Linaro/tuxmake/
site_url: "https://tuxmake.org/"
theme:
  name: material
  logo: tuxmake.svg
  favicon: tuxmake_icon.svg
  font: false
  features:
    - content.code.copy
    - navigation.footer
markdown_extensions:
  - mkautodoc
extra_css:
  - mkautodoc.css
  - extra.css

nav:
- Getting Started: index.md
- Installing TuxMake:
  - PyPI: install-pypi.md
  - Debian packages: install-deb.md
  - RPM packages: install-rpm.md
  - Run uninstalled: run-uninstalled.md
- Features:
  - Make Targets: targets.md
  - Target Architectures: architectures.md
  - Toolchains: toolchains.md
  - Kernel Configuration: kconfig.md
  - Compiler Wrappers: wrappers.md
  - Runtimes: runtimes.md
  - Metadata: metadata.md
  - Reproducible Builds: reproducible_builds.md
  - Curated Containers: curated_containers.md
  - Custom Containers: custom_containers.md
- Reference:
  - Command Line Reference: cli.md
  - Python API Reference:
    - Build: python.md
    - Runtime Library: python_runtime.md
- Community:
  - Chat: chat.md
  - Code of Conduct: code-of-conduct.md
  - Contributing: contributing.md
  - Downstream Packaging: packaging.md
  - License: license.md
- External resources: resources.md
